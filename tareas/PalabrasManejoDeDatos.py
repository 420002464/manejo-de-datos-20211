#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: manuel
"""

Semana 1 : python, spyder, clase, objeto, atributo, método, variables, funciones, bloque, identacion, palabras reservadas, interprete, ejecutar,
str, self, init, constructor
semana 2:clase, algoritmo MCD, ciclo, while, pass, ejecutar, método, atributos privados, setter, getter, this, objeto, constructor
semana 3: python, metodos, objeto, funcion, impar, self, ejecutar, script, modulo, cadena, main, asignacion, comparacion, name, pass, privado,
publico, init, dunders
semana 4: python, comandos, operadores, print, asignacion, comparacion, listas, cadenas, inmutable, help(object), append, len, for,
if, while, break, diccionario, key, condicionales, gitAhead, clonar, repositorio, directorio, commit, push, graficas de araña, atracones, repulsores,
matplotlib.pyplot, graficas, biblioteca, matrices, numpy, sqrt, np log()
semana 5: clase, self, metodo, asignacion, algoritmos, problema de transporte, esquina noroeste, fila, columna, paqueteria, pretty table,
anaconda, ejecutar, administrador, instalacion, encapsulamiento, atributos, diccionario, atributo privado, str, dunder, lista
semana 6: script, ejecutar, bloque, codigo, depurar, ruptura, datos, metodos, funcion, explorador, variables, print, clase, git, terminal,
debug, fantasmas, self.get, lista, objetos, self, iterable, from, import, cliente, diccionario, .set, status, add, overleaf, editor de texto,
algoritmo caminos minimos, nodo, aristas, URI
semana 7: aleatorios, importar, programacion orientada a objetos, codigo, analisis, Dijkstra, setters, getters, objeto, metodo, str, cadena,
peso, depuracion, str, repr, grafica, funcion, lista, append, return
semana 8: clase, metodo, optimo, minimo, set, get, Dijkstra, str, repr, run-step, método de costo mínimo, sorted, lista, estructura, sort, sorteo
semana 9: Dijkstra, run_step, método, run, algoritmo, recuperaCamino, listas, indice, sorted, celdas, print, ejecutar,diccionario, dic.item(),
ipdb, pdb, try, except, raise, errores
semana 10: metodo costo-minimo, analisis, ejecucion, order, hubble, sort, metodo binario, recursividad
semana 11: compara, algoritmo Bubblesort, traducir, arreglo, ciclo, for, if, len, heap sort, counting sort, algoritmo de ordenamiento,
merge sort, quick sort, insertation sort, comparan, range, intercambiar, while, key, recursividad 
semana 12: lista, ciclo, sucesion, fibonacci, recursivo, datos, funcion, implementacion, factorial, mergelistas, comparar, suma, else,
agrupacion, ciclos
semana 13: Dijkstra, algoritmos de ordenamiento, valores aleatorios, seed(), random, módulo, secuencia, quicksort, pivote, lista, int, rango
semana 14: valor aleatorio, intervalo, probabilidad, lista, lista por comprensión, iterable, random, distribuciones, randint, gauss, quicksort,
método recursivo, numpy, arreglos, matrices
semana 15: choice, elemento aleatorio, secuencia no vacía, lista, repeticion, valores aleatorios unicos, sample, shuffle, permuta,
factorial, recursiva, stack, quicksort, particiona, busqueda secuencial, SQL, busqueda binaria, cliente, aplicación, servidor, base de datos,
show data bases, use world, desc city, show table
