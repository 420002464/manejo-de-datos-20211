#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: manuel
"""

p1:¿qué es una clase y un objeto?
r1:una clase es lo que nos ayudará a la creación de objetos, pues la clase es en términos simples el plano o plantilla para la creación de objetos. La clase funciona a través de métodos. Un objeto es el caso particular, con ciertos valores de la clase
p2:¿qué son las palabras reservadas?
r2:son palabras que ya tienen un uso específico en el lenguaje de programacion y una máxima del curso es no modificarlas
p3:¿qué elementos debe tener toda clase?
r3: constructor, str, setter, getter y los métodos
p4:¿para qué sirve el setter y el getter?
r4: el primero para cambiar el valor de los datos y el otro para obtener el valor de los datos
p5:¿a qué hace referencia self?
r5:A los valores de la variable que modifica
p6:¿Qué diferencias hay entre lo público y lo privado?
r6:Lo público se puede consultar y modificar. Lo privado no se puede modificar en cualquier momento.
p7:¿para qué sirve depurar?
r7:Para la identificacion de errores lógicos de un programa.
p8:¿cuál es el uso de try,except y raise?
r8: Probar un bloque en busqueda de errores  Permite manejar el error   levantar una excepcion en particular
p9:¿qué son las listas por comprensión?
r9:Es una lista hecha a partir de otros elementos iterables.
p10: ¿Qué significa SQL?
r10: Simple query lenguage
p11:¿que es sql?
r11: Es el lenguaje de las bases de datos