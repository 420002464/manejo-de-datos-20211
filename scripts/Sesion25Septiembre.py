# -*- coding: utf-8 -*-
"""
Spyder Editor

"""

class racional():
    def __init__(self, numerador, denominador):
        #self.p = numerador
        #self.q = denominador
        self.setP(numerador)
        self.setQ(denominador)
        
    def __str__(self):
        return "{}/{}".format(self.p,self.q)
    
    def __mul__(self,r1):
        return racional(self.getP() * r1.p, self.getQ() * r1.q)
    
    def setP(self,numerador):
        if isinstance(numerador,int):
            self.__p = numerador
        else:
            print("El numerador debe ser entero")
    
    def setQ(self,denominador):
        if isinstance(denominador,int) and denominador > 0:
            self.__q = denominador
        else:
            print("El denominador debe ser entero distinto de 0")
    
    def getP(self):
        return(self.getP())
    
    def getQ(self):
        return(self.getQ())    
    
    def es_propia(self):
        return self.getP() < self.getQ()
    
    def es_impropia(self):
        return self.getP() > self.getQ()
    
    def simplificar(self):
        m = mcd(self.getP(),self.getQ())
        self.setP(self.getP()/m)
        self.setQ(self.getQ()/m)
        return ()
        
def mcd(a,b):
    r = a%b
    while r:
        a = b
        b = r
        r = a%b
    return(b)

#a = racional(1,2)
#print(a)
#print(a.p)
#print('El valor de p es :' + str(a.p))

