#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: manuel
"""

l = [5,6,5,4,8]
resultado = 0

for i in range(len(l)):
    resultado = resultado + l[i]
    
print(resultado)

def suma(l):
    resultado = 0
    for i in range(len(l)):
        resultado = resultado + l[i]
    return(resultado)

print(suma(l))

def sumaR(l):
    if len(l) == 0:
        return(0)
    else:
        return(l[0] + sumaR(l[1:]))
    
    
def Fibo(n):
    l = [0,1]
    if n == 0:
        l = [0]
    elif n > 1:
        for i in range(n-1):
            l.append(l[-1]+l[-2])
    return(l)

print(Fibo(10))

def sumaRec(l):
    if len(l) == 0:
        return(0)
    else:
        return(l[-1] + sumaR(l[:-1]))
    
print(sumaRec(l))

def Facto(n):
    if n==0:
        return(1)
    return(n*Facto(n-1))
