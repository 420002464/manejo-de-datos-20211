import mysql.connector
from mysql.connector import errorcode

DB_NAME = 'halm_ManejoDeDatos'

TABLES = {}

TABLES['preguntas_respuestas'] = (
    "CREATE TABLE `preguntas_respuestas` ("
    "  `no_pregunta` int(2) NOT NULL AUTO_INCREMENT,"
    "  `pregunta` varchar(200) NOT NULL,"
    "  `respuesta` varchar(30) NOT NULL,"
    "  PRIMARY KEY (`no_pregunta`)"
    ") ENGINE=InnoDB")

TABLES['palabras_y_conceptos'] = (
    "CREATE TABLE `palabras_y_conceptos` ("
    "  `no_semana` int(2) NOT NULL AUTO_INCREMENT,"
    "  `palabras` varchar(300) NOT NULL,"
    "  PRIMARY KEY (`no_semana`)"
    ") ENGINE=InnoDB")

config = {
  'user': 'halm',
  'password': '420002464',
  'host': 'mysql-17763-0.cloudclusters.net',
  'port': '17792',
  'database': 'halm_ManejoDeDatos',
  'raise_on_warnings': True
}

cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()

try:
    cursor.execute("CREATE DATABASE IF NOT EXISTS " + DB_NAME)
except mysql.connector.Error as err:
    print("La base de datos " + DB_NAME + " ya existe")
    print(err.msg)
else:
    print("Se ha creado la base de datos " + DB_NAME )

cursor.execute("USE " + DB_NAME)

for table_name in TABLES:
    table_description = TABLES[table_name]
    try:
        print("Creando tabla {}: ".format(table_name), end='')
        cursor.execute(table_description)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("Ya existe.")
        else:
            print(err.msg)
    else:
        print("OK")

add_pregunta = ("INSERT INTO preguntas_respuestas"
               "(pregunta,respuesta)"
               "VALUES (%s, %s)")

add_semana = ("INSERT INTO palabras_y_conceptos"
               "(palabras)"
               "VALUES (%s)")

pregunta1 = ('¿qué es una clase y un objeto?','una clase es lo que nos ayudará a la creación de objetos, pues la clase es en términos simples el plano o plantilla para la creación de objetos. La clase funciona a través de métodos. Un objeto es el caso particular, con ciertos valores de la clase')
pregunta2 = ('¿qué son las palabras reservadas?','son palabras que ya tienen un uso específico en el lenguaje de programacion y una máxima del curso es no modificarlas')
pregunta3 = ('¿qué elementos debe tener toda clase?','constructor, str, setter, getter y los métodos')
pregunta4 = ('¿para qué sirve el setter y el getter?','el primero para cambiar el valor de los datos y el otro para obtener el valor de los datos')
pregunta5 = ('¿a qué hace referencia self?','A los valores de la variable que modifica')
pregunta6 = ('¿Qué diferencias hay entre lo público y lo privado?','Lo público se puede consultar y modificar. Lo privado no se puede modificar en cualquier momento.')
pregunta7 = ('¿para qué sirve depurar?','Para la identificacion de errores lógicos de un programa.')
pregunta8 = ('¿cuál es el uso de try,except y raise?','Probar un bloque en busqueda de errores  Permite manejar el error   levantar una excepcion en particular')
pregunta9 = ('¿qué son las listas por comprensión?','Es una lista hecha a partir de otros elementos iterables.')
pregunta10 = ('¿Qué significa SQL?','Simple query lenguage')
pregunta11 = ('¿que es sql?','Es el lenguaje de las bases de datos')


data_preguntas = [pregunta1,pregunta2,pregunta3,pregunta4,pregunta5,pregunta6,pregunta7,pregunta8,pregunta9,pregunta10,pregunta11]

for i in data_preguntas:
    cursor.execute(add_pregunta, i)
    no_pregunta = cursor.lastrowid
cnx.commit()

Semana1 = ('python, spyder, clase, objeto, atributo, método, variables, funciones, bloque, identacion, palabras reservadas, interprete, ejecutar,str, self, init, constructor')
Semana2 = ('clase, algoritmo MCD, ciclo, while, pass, ejecutar, método, atributos privados, setter, getter, this, objeto, constructor')
Semana3 = ('python, metodos, objeto, funcion, impar, self, ejecutar, script, modulo, cadena, main, asignacion, comparacion, name, pass, privado, publico, init, dunders')
Semana4 = ('python, comandos, operadores, print, asignacion, comparacion, listas, cadenas, inmutable, help(object), append, len, for, if, while, break, diccionario, key, condicionales, gitAhead, clonar, repositorio, directorio, commit, push, graficas de araña, atracones, repulsores, matplotlib.pyplot, graficas, biblioteca, matrices, numpy, sqrt, np log()')
Semana5 = (' clase, self, metodo, asignacion, algoritmos, problema de transporte, esquina noroeste, fila, columna, paqueteria, pretty table, anaconda, ejecutar, administrador, instalacion, encapsulamiento, atributos, diccionario, atributo privado, str, dunder, lista')
Semana6 = ('script, ejecutar, bloque, codigo, depurar, ruptura, datos, metodos, funcion, explorador, variables, print, clase, git, terminal, debug, fantasmas, self.get, lista, objetos, self, iterable, from, import, cliente, diccionario, .set, status, add, overleaf, editor de texto, algoritmo caminos minimos, nodo, aristas, URI')
Semana7 = ('aleatorios, importar, programacion orientada a objetos, codigo, analisis, Dijkstra, setters, getters, objeto, metodo, str, cadena, eso, depuracion, str, repr, grafica, funcion, lista, append, return')
Semana8 = ('clase, metodo, optimo, minimo, set, get, Dijkstra, str, repr, run-step, método de costo mínimo, sorted, lista, estructura, sort, sorteo')
Semana9 = ('Dijkstra, run_step, método, run, algoritmo, recuperaCamino, listas, indice, sorted, celdas, print, ejecutar,diccionario, dic.item(), ipdb, pdb, try, except, raise, errores')
Semana10 = ('metodo costo-minimo, analisis, ejecucion, order, hubble, sort, metodo binario, recursividad')
Semana11 = ('compara, algoritmo Bubblesort, traducir, arreglo, ciclo, for, if, len, heap sort, counting sort, algoritmo de ordenamiento, merge sort, quick sort, insertation sort, comparan, range, intercambiar, while, key, recursividad ')
Semana12 = ('lista, ciclo, sucesion, fibonacci, recursivo, datos, funcion, implementacion, factorial, mergelistas, comparar, suma, else, agrupacion, ciclos')
Semana13 = ('Dijkstra, algoritmos de ordenamiento, valores aleatorios, seed(), random, módulo, secuencia, quicksort, pivote, lista, int, rango')
Semana14 = ('valor aleatorio, intervalo, probabilidad, lista, lista por comprensión, iterable, random, distribuciones, randint, gauss, quicksort, método recursivo, numpy, arreglos, matrices')
Semana15 = ('choice, elemento aleatorio, secuencia no vacía, lista, repeticion, valores aleatorios unicos, sample, shuffle, permuta, factorial, recursiva, stack, quicksort, particiona, busqueda secuencial, SQL, busqueda binaria, cliente, aplicación, servidor, base de datos, show data bases, use world, desc city, show table')
Semana16 = ('bases, datos, manejador, SQL, MySQL, tabla, puertos, servidor, almacenamiento, variable, llave, campo, tipo de dato, null, extra, default, Simpy, simular, procesos, ambiente, shell, enum, smallint, valor')

data_semana = [Semana1,Semana2,Semana3,Semana4,Semana5,Semana6,Semana7,Semana8,Semana9,Semana10,Semana11,Semana12,Semana13,Semana14,Semana15,Semana16]

for i in data_semana:
    cursor.execute(add_semana, (i,))
    no_semana = cursor.lastrowid
cnx.commit()

cursor.close()
cnx.close()
