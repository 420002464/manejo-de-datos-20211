#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 13:55:25 2021

@author: luis
"""

import datetime
import mysql.connector

config = {
  'user': 'halm',
  'password': '420002464',
  'host': 'mysql-17763-0.cloudclusters.net',
  'port': '17792',
  'database': 'halm_employee',
  'raise_on_warnings': True
}

cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()


consulta = ("SELECT first_name, last_name, hire_date FROM employees ")



hire_start = datetime.date(1999, 1, 1)
hire_end = datetime.date(1999, 12, 31)
# query = ("SELECT first_name, last_name, hire_date FROM employees "
#          "WHERE hire_date BETWEEN %s AND %s")
cursor.execute(consulta)

for (first_name, last_name, hire_date) in cursor:
  print("{}, {} fue contratado el {:%d %b %Y}".format(
    last_name, first_name, hire_date))

cursor.close()
cnx.close()
