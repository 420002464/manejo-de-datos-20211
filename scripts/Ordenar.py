#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""


@author: manuel
"""

l = [7, 3, 5, 9, 2]
n = len(l)

if l[0] > l[1]:
    aux = l[0]
    l[0] = l[1]
    l[1] = aux
    
for j in range(n-1):
    for i in range(n-1):
        if l[i] > l[i+1]:
            aux = l[i]
            l[i] = l[i+1]
            l[i+1] = aux
    n -= 1

def BubbleSort(l):
    n = len(l)
    for j in range(n-1):
        for i in range(n-1):
            if l[i] > l[i+1]:
                aux = l[i]
                l[i] = l[i+1]
                l[i+1] = aux
        n -= 1
        
def ordenar(l):
    n = len(l)
    for j in range(n-1):
        for i in range(n-1):
            if l[i] > l[i+1]:
                l[i], l[i+1] = l[i+1], l[i]
        n -= 1

li =[4.7,9,7,2,1]
print(li)
ordenar(li)
print(li)

# def burbuja_bidireccional(l):
#     ultimo = len(l)
#     a = len(l)%2
#     primero = 0
#     for j in range(len(l)-1):
#         if a==1 and j%2!=a or a==0 and j%2==a:
#             for i in range(primero,ultimo-1):
#                 if l[i] > l[i+1]:
#                     l[i], l[i+1] = l[i+1], l[i]
#                 ultimo -= 1
#             else:
#                 for i in range(ultimo-1,primero,-1):
#                     if l[i] < l[i-1]:
#                         l[i-1], l[i] = l[i], l[i-1]
#                 primero += 1
#     return
 
    
def bd(l):
    n = len(l)
    for j in range(n-1):
        for i in range(n-1):
            if l[i] > l[i+1]:
                l[i], l[i+1] = l[i+1], l[i]
        for i in range(1,n):
            if l[i] < l [i-1]:
                l[i-1], l[i] = l[i], l[i-1]
        n -= 1
    
 
l = [5,9,1,7,3,7,2,1,7,6]               
        
print(l)
bd(l)
print(l)
