#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""


@author: manuel
"""


# Con el algoritmo de Euclides

def alEu(a,b):
    r = a%b
    while r:
        a = b
        b = r
        r = a%b
    return(b)


# Con un ciclo for y fuerza bruta

def mcd(a,b):
    for i in range(1,a+1):
        if (a%i == 0) and (b%i == 0):
            c = i
    return(c)
