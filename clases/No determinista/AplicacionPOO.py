#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 16:03:36 2021

@author: manuel
En el código se tranajará el métdodo de rechazo, o sea obtener una aproximación al valor de una integral definida 
 con el método montecarlo
"""
from math import sin, pi,exp
from random import random
from scipy.integrate import quad
print("Debe ejecutar la clase de la sig manera (numero de puntos,extremo izquierdo,extremo derecho,funcion con comillas)")

class montecarlo():
    def __init__(self,num_puntos,extremo_izq,extremo_derecho,func):
        
        self.set_num_puntos(num_puntos)
        self.set_a(extremo_izq)
        self.set_b(extremo_derecho)
        self.set_f(func)
    
    def __str__(self):
        if self.f(self.get_b())<=self.f(self.get_a()):
            d=self.integralfdecrec()
        elif self.f(self.get_a())<=self.f(self.get_b()):
            d=self.integralfcrec()
        cadena="La aproximación al área bajo la curva de: "+self.get_f()+" es: "+str(d)+" con un error de aproximación de: "+str(self.error())+"%"
        return cadena
        
    def set_num_puntos(self,num_puntos):
        self.__num_puntos=num_puntos
        
    def set_a(self,extremo_izq):
        self.__a=extremo_izq
        
    def set_b(self,extremo_derecho):
        self.__b=extremo_derecho
    
    def set_f(self,func):
        self.__f=func
    
    def get_num_puntos(self):
        return self.__num_puntos
    
    def get_a(self):
        return self.__a
    
    def get_b(self):
        return self.__b
    
    def get_f(self):
        return self.__f
    #def set
    
    def puntos(self):
        
        l=[]
        #e=[]
        for _ in range(self.get_num_puntos()):
            c = [2*random()-1,2*random()-1] #circulo unitario anclado al origen
            if (c[0]**2+c[1]**2)**(1/2)<=1:
                l.append(c[0])
            
                
        return(4*len(l)/self.get_num_puntos())   #la proporcion de puntos que cayeron en la circunferencia entre 
        #los que cayeron en el cuadrado inscrito
        
        # en f se busca que el usuario ingrese una función conociendo la notación para que posteriormente sea evaluada en el intervalo dado
    def f(self,x): #se cambio el self,x por solo self
        f = self.get_f()
        
        y=eval(f) 
        return y
        #sin(x)
    def integralfcrec(self):
        
        A=[]
        
        for _ in range(self.get_num_puntos()):
            x=(self.get_b()-self.get_a())*random()+self.get_a() #el intervalo del dominio
            y=random()*self.f(self.get_b())  # el intervalo del contradominio, recordemos que la integral de una funcion en el eje y es desde f(x) hasta el eje X
            if y <= self.f(x):
                A.append(x)
        return ((self.get_b()-self.get_a())*(self.f(self.get_b()))*len(A)/self.get_num_puntos())
        #j=(len(A)/self.experimento)
    
    def integralfdecrec(self):
        B=[]
        for _ in range(self.get_num_puntos()):
            x=(self.get_b()-self.get_a())*random()+self.get_a()
            y=random()*self.f(self.get_a())
            if y<= self.f(x):
                B.append(x)
            return((self.get_b()-self.get_a())*(self.f(self.get_a()))*len(B)/self.get_num_puntos())
            
        #B=[]
        
    def error(self):
        if self.f(self.get_b())<=self.f(self.get_a()):
            k=self.integralfdecrec()
        elif self.f(self.get_a())<=self.f(self.get_b()):
            k=self.integralfcrec()
        f=lambda x:eval(self.get_f()) 
        a=(abs(k-quad(f,self.get_a(),self.get_b())[0])/quad(f,self.get_a(),self.get_b())[0])*100
        return a
    
    
    def aleatorios(self):
        P=[["sin(x)",1500,0,1],["x**self.get_b()",1250,0,3],["x**3",4000,0,3],["x**2",2560,2,10],["cos(x)",3000,0,1],["exp(x)",5000,0,exp(1)],["1/x",1500,1,2],["1/x**2",2000,1,3],["8*x**4+3*x",3000,2,8],["1/5*x**5+7",5000,1,4]]
        j=P[int(random()*10)-1]
            
        if j==P[4] or P[6] or P[7]:
            return montecarlo.integralfdecrec(j[1],j[2],j[3],j[0])
        else:
            return montecarlo.integralfcrec(j[1],j[2],j[3],j[0])
            
            
        
        
p=montecarlo(1000,0,1,"x**2")
print(p)

"""
    def integralcota(self):#necesito una variable nueva
        C=[]
        for _ in range(self.get_num_puntos()):
            x=(self.get_b()-self.get_a())*random()+self.get_a()
            y=random()*d#nueva variable
            if y<= self.f(x):
                C.append(x)
            return((self.get_b()-self.get_a())*d*len(C)/self.get_num_puntos())
    
        
        """
        
    
"""en este método se obtendrá el error de la aproximación con los módulos que nos proporciona python
        """
"""
    def integral(f,n,a,b):
    
    A=[]
    
    for _ in range(n):
        v=(b-a)*random()+a  #el intervalo del dominio
        y=random()*(b)**2   # el intervalo del contradominio, recordemos que la integral de una funcion en el eje y es desde f(x) hasta el eje X
        #print(y)
        if y <= v**2:
            A.append(v)
    return ("el Área aproximada es:",len(A)/n), len(A)
    """
"""
    def f(x):
    x**2
    #sin(x)

def integral(f,n,a,b):
    
    A=[]
    
    for _ in range(n):
        v=(b-a)*random()+a  #el intervalo del dominio
        y=random()*sin(b)   # el intervalo del contradominio, recordemos que la integral de una funcion en el eje y es desde f(x) hasta el eje X
        #print(y)
        if y <= sin(v):
            A.append(v)
    return ("el Área aproximada es:",(b-a)*(sin(b))*len(A)/n), len(A) 
        EN EL INCISO ANTERIOR LO QUE SE CALCULABA ERA LA PROPORCION, PERO AL SER EL AREA TOTAL 1, ENTONCES LA PROPORCION COINCIDIA CON
        EL AREA, EN ESTA VERSION SE CONSIDERA EL AREA TOTAL Y ESTA SE DESPEJA PARA ASI OBTENER EL AREA APROXIMADA BAJO LA CURVA
    """

                
            
        
        
        
            