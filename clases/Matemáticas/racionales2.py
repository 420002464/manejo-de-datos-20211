#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: manuel
"""

class racional():
  def __init__(self, numerador, denominador):
    #self.p = numerador
    #self.q = denominador
    self.setP(numerador)
    self.setQ(denominador)

  def __str__(self):
    return "{} / {}".format(self.p,self.q)

  def __mul__(self, r1):
    return racional(self.p * r1.p, self.q*r1.q) #directa
    return racional(self.getP() * r1.getP(), self.getQ()*r1.getQ()) # indirecta

  def setP(self,numerador):
    if isinstance(numerador, int):
      self.p = numerador
    else:
      print("El numerador debe ser entero")    

  def setQ(self, denominador):
    if isinstance(denominador,int) and denominador!=0 and denominador > 0:
      self.q = denominador
    else:
      print("El denominador debe ser diferente de cero y entero")

  def getP(self):
    return self.p
    
  def getQ(self):
    return self.q

  def simplificar(self):
    m = mcd(self.getP(), self.getQ()) 
    self.setP(self.getP()/m)
    self.setQ(self.getQ()/m)
     
  def es_propia(self):
    return self.p <  self.q
  def es_impropia(self):
    return self.p >  self.q    
  def mcd(self, a, b):
    pass

def mcd(a, b):
  pass



class complejo():
  def __init__(self, parte_real, parte_imaginaria):
    self.a = parte_real
    self.b = parte_imaginaria

  def __str__(self):
    return "{} + i{}".format(self.a,self.b)

a = racional(1, 2)
print(a)
a.p = 12
print(" El valor de p es :" + str(a.p))
print(" El valor de p es :" + str(a.getP()))

b = racional("uno", "dos")
print(b)
c = racional("uno", 0)
print(c)
d = racional(3,4)
e = a * d
# e = a.__mul__(d) 
print(e)
print(e.es_propia())
print(e.es_impropia())

t = complejo(1,0)
print(t)

