#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 08:14:10 2020

@author: manuel
"""
#Vamos a repasar la programación orientada a objetos, en este caso con el ejemplo de los números racionales
class racional():
    #la razón por la cual hay un ":" no es xq sea como en C o C++, ya que Python es identado, sino por que sirve para iniciar
    # un nuevo bloque, además del inicio de la definición de clase
    def __init__(self, numerador, denominador):# esto se llama métodos Dunder ej: el init y el str
        #se abrió otro bloque en donde se define cómo es que se construye el objeto racional
        #self.p=numerador
        #self.q=denominador
        self.setP(numerador)
        self.setQ(denominador)
        #este es nuestro constructor, se necesitan dos valores para construir el racional
    def __str__(self):
        return "{}/{}".format(self.getP(),self.getQ())
    def __mul__(self, r1):
        #directa
        return racional(self.p*r1.p, self.q*r1.q)
        #indirecta  
    def setP(self,numerador):
        if isinstance(numerador,int):
            self.p=numerador
        else:
            print("el denominador debe ser 0")
        
    def setQ(self,denominador):
        if isinstance(denominador,int) and denominador != 0:
            self.q=denominador
        else:
            print("El entero debe ser diferente de 0 y entero")
    def getP(self):
        return self.p
    def getQ(self):
        return self.q
    def es_propia(self):#def corresponde al método
        return self.p < self.q
    def es_impropia(self):
        return self.p > self.q
    def simplificar(self):
        m = mcd(self.getP(),self.getQ())
        self.setP(self.getP()/m)
        self.setQ(self.getQ()/m)
        #como metodo
    #def mcd (self,a,b):
        #pass es para que aunque no implementes un metodo no aparezca un error
        #pass
def mcd(a,b):
    while a % b !=0:
        resto= a % b
        a=b
        b=resto
    return b
        
    
"""
como funcion
def mcd(a,b):
"""
if __name__ == "__main__":
    a=racional(1,2)
    print(a)
    a.p=12
    print("el valor de p es:"+ str(a.p))
    print("el valor de p es:"+ str(a.getP()))
    #cuando son privados no son permitidos los siguientes llamados
    #print("el valor de p es:"+ str(a.__p))
    
    """Pero qué pasa si..."""
    b=racional("uno","dos")
    print(b)
    c=racional("uno",0)
    print(c)
    # no debería pasar, asi que hay que poner restricciones al objeto que queremos crear, para que no de cabida a cosas sin sentido
    #o no posibles definir
    d = racional(3,4)
    e= a * d
    #e=a.__mul__(d) es una forma más parecida a Java
    print(e)
    print(e.es_propia())
    print(e.es_impropia())
    f=racional(15,21)
print("La variable __name__ en el archivo main,py contiene la cadena" + __name__)

