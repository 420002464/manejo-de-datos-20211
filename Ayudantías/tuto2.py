# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 01:43:03 2021

@author: ASUS
"""

import simpy
#simula un coche eléctrico que necesita carga y se conduce
#interacciones entre procesos

class Car(object):
     def __init__(self, env):
         self.env = env
         # Start the run process everytime an instance is created.
         # Que empiece a ejecutarse una vez siendo creada
         self.action = env.process(self.run())
         #le digo a mi objeto que empiece
     def run(self):
         while True:
             print('Start parking and charging at: %d' % self.env.now)
             charge_duration = 5
             #que se cargue por 5 unidades de tiempo
             # We yield the process that process() returns
             # to wait for it to finish
             yield self.env.process(self.charge(charge_duration))
#hasta que termine de cargarse se puede ir a otro evento dentro del proceso
             # The charge process has finished and
             # we can start driving again.
             print('Start driving at: %d' % self.env.now)
             trip_duration = 2
             #maneja por dos unidades de tiempo 
             yield self.env.timeout(trip_duration)
             #Deja de manejar hasta que alcances el tiempo de viaje

     def charge(self, duration):
         yield self.env.timeout(duration)
         #termina la carga después del tiempo determinado
         
#Mismas intrucciones  de tuto1.py ejemplo auqnue cambian debido a que es una clase
env = simpy.Environment()
car = Car(env)
env.run(until = 15)
#se ejecute hasta estar en 15 unidades de tiempo