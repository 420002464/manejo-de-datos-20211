#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 08:15:44 2021

@author: manuel
"""
def quicksortR(array,l,h):
    if l < h:
        posicionPivote = particiona(array,l,h)
        quicksortR(array,l,posicionPivote-1)
        quicksortR(array,posicionPivote+1,h)
        
def quicksortI(array):
    stack = []
    l = 0
    h = len(array)-1
    stack.append((l,h))
    while stack:
        l,h = stack.pop()
        posicionPivote = particiona(array,l,h)
        if l < posicionPivote-1:
            stack.append((l,posicionPivote-1))
        if h > posicionPivote+1:
            stack.append((posicionPivote+1,h))

def particiona(array,l,h):
    pivote = array[h]
    i=l
    j=h-1
    while i<= j: #mientras no se crucen
        #en el caso en que array[i]> pivote  y array[j]<pivote
        #se hace el swap y se aumenta i y se decrementa j
        if array[i]> pivote and array[j] <= pivote:
            temp = array[i] 
            array[i]=array[j]
            array[j]=temp
            i=i+1
            j=j-1
        
        # en el caso que valor de array[i]> pivote  y   array[j]>pivote
        #se decrementa j
        if array[i] > pivote and array[j] > pivote:
            j=j-1
        
        #el caso en ek que valor array [i]<pivote y array [j]< pivote
        #se aumenta i
        if array[i] <= pivote and array[j] <= pivote:
            i=i+1
        
        # en el caso que valor array[i]<pivovte y array [j]>pivote
        #se aumenta i y se decrementa j
        if array[i] <= pivote and array[j] > pivote:
            i=i+1
            j=j-1
    temp=array[i]
    array[i]=pivote
    array[h]=temp
    return i
    
arr = [5,3,7,6,2,1,4]
l=0
h=len(arr)-1
print(arr)
particiona(arr,l,h)
print(arr)
