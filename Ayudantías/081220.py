#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""


@author: manuel
"""

def facto(n):
    if n == 0:
        return(1)
    else:
        return(n*facto(n-1))
    
def MergeListas(lista_izquierda, lista_derecha):
    nl = []
    while len(lista_izquierda)>0 and len(lista_derecha)>0:
        if min(lista_izquierda) < min(lista_derecha):
            nl.append(min(lista_izquierda))
            lista_izquierda.remove(min(lista_izquierda))
        else:
            nl.append(min(lista_derecha))
            lista_derecha.remove(min(lista_derecha))
    while len(lista_izquierda)>0:
        nl.append(min(lista_izquierda))
        lista_izquierda.remove(min(lista_izquierda))
    while len(lista_derecha)>0:
        nl.append(min(lista_derecha))
        lista_derecha.remove(min(lista_derecha))
    return(nl)
        
def MergeSort(lista):
    if len(lista) <= 1:
        return(lista)
    iM = len(lista)//2
    lizq = lista[:iM]
    lder = lista[iM:]
    lizqO = MergeSort(lizq)
    lderO = MergeSort(lder)
    listaordenada = MergeListas(lizqO,lderO)
    return(listaordenada)

lO = [7,9,1,7,74,28,61,7,6,18,61,8,94,5]
lO = MergeSort(lO)
print(lO)
