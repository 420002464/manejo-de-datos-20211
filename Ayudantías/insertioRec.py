    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 08:35:00 2021

@author: manuel
"""

def insertionSortRecursivo(arr,n):
    if n<= 1:
        return
    
    insertionSortRecursivo(arr,n-1)
    ultimo=arr[n-1]
    j=n-2
    
    while(j>=0 and arr[j]>ultimo):
        arr[j+1]=arr[j]
        j=j-1
        arr[j+1]=ultimo
        
arr=[2,8,5,3,9,4]
n1=len(arr)
insertionSortRecursivo(arr,n1)

for i in range(n1):
    print(arr[i], end="")
