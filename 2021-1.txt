manejo-de-datos-20211
├── manejo-de-datos-20211/Ayudantías
│   ├── manejo-de-datos-20211/Ayudantías/081220.py Script con implementaciones recursivas del algoritmo de ordenamiento "Merge Sort" y la función factorial.
│   ├── manejo-de-datos-20211/Ayudantías/101220.py Script con una implementación iterativa del algoritmo de ordenamiento "Merge Sort".
│   ├── manejo-de-datos-20211/Ayudantías/busqueda.py Implementación de funciones para búsqueda secuencial y búsqueda binaria.
│   ├── manejo-de-datos-20211/Ayudantías/Dijkstra.py Implementacion Dijkstra con las clases las clases "vertice", "arista", "grafica" y "dijkstra".
│   ├── manejo-de-datos-20211/Ayudantías/DijkstraT.jpg Foto de la actividad dejada por Karla donde se usa el método Dijkstra
│   ├── manejo-de-datos-20211/Ayudantías/EscenarioS.txt El archivo .txt con la idea de una simulacion que se podria hacer con simpy
│   ├── manejo-de-datos-20211/Ayudantías/Insertion.py Implementacion iterativa de insertion sort
│   ├── manejo-de-datos-20211/Ayudantías/insertioRec.py Implementacion recursiva de insertion sort
│   ├── manejo-de-datos-20211/Ayudantías/mergeiterativoconerror.py Script con las implementaciones iterativa y recursiva del algoritmo de ordenamiento Quick Sort con error
│   ├── manejo-de-datos-20211/Ayudantías/mergeiterativo.py Script con las implementaciones iterativa y recursiva del algoritmo de ordenamiento Quick Sort
│   ├── manejo-de-datos-20211/Ayudantías/ProblemaDelTransporte.py Problema del transporte se definieron las clases ciudad, planta y problema_transoprte, y en esta última los métodos de la esquina noroeste, del costo mínimo por ciudad, del costo mínimo global
│   ├── manejo-de-datos-20211/Ayudantías/quicksort.py Script con las implementaciones iterativa y recursiva del algoritmo de ordenamiento "Quick Sort".
│   ├── manejo-de-datos-20211/Ayudantías/readme.txt el archivo que se lee para saber de que trata este directorio
│   ├── manejo-de-datos-20211/Ayudantías/taquillaEjemploClase.py  aplicacion de simpy y se simula una taquilla
│   ├── manejo-de-datos-20211/Ayudantías/tuto1.py Tutoria de simpy
│   ├── manejo-de-datos-20211/Ayudantías/tuto2.py Tutoria de simpy
│   └── manejo-de-datos-20211/Ayudantías/tuto3.py Tutoria de simpy
├── manejo-de-datos-20211/clases
│   ├── manejo-de-datos-20211/clases/ido
│   │   └── manejo-de-datos-20211/clases/ido/ProblemaDelTransporte.py Archivo que en el que se definieron las clases ciudad, planta y problema_transoprte, y en esta última los métodos de la esquina noroeste, del costo mínimo por ciudad, del costo mínimo global
│   ├── manejo-de-datos-20211/clases/Matemáticas
│   │   ├── manejo-de-datos-20211/clases/Matemáticas/complejos.py En este módulo se definio la clase "complejos"
│   │   ├── manejo-de-datos-20211/clases/Matemáticas/racionales2.py Es el primer módulo en el que se definió la clase "racionales"
│   │   └── manejo-de-datos-20211/clases/Matemáticas/racionales.py El segundo módulo donde se definió la clase "racionales", en este módulo también se empezó a definir la clase "complejos" pero no se completó
│   ├── manejo-de-datos-20211/clases/No determinista
│   │   └── manejo-de-datos-20211/clases/No determinista/AplicacionPOO.py Es la clase que definimos para resolver el problema que quisieramos
│   └── manejo-de-datos-20211/clases/readme.txt Archivo donde contiene la descripción del directorio
├── manejo-de-datos-20211/ejemplos
│   └── manejo-de-datos-20211/ejemplos/readme.txt Archivo donde contiene la descripción del directorio
├── manejo-de-datos-20211/extras
│   └── manejo-de-datos-20211/extras/readme.txt Archivo donde contiene la descripción del directorio
├── manejo-de-datos-20211/LaTeX
│   ├── manejo-de-datos-20211/LaTeX/Introducción a LaTeX
│   │   ├── manejo-de-datos-20211/LaTeX/Introducción a LaTeX/miPrimerDocumento.tex Tarea opcional dejada por Sandra, son preguntas
│   │   │── manejo-de-datos-20211/LaTeX/Introducción a LaTeX/miPrimerDocumento.pdf  El pdf de la tarea opcional
│   ├── manejo-de-datos-20211/LaTeX/readme.txt Archivo donde contiene la descripción del directorio
│   └── manejo-de-datos-20211/LaTeX/resumen_global1
│       ├── manejo-de-datos-20211/LaTeX/resumen_global1/imagenes
│       │   ├── manejo-de-datos-20211/LaTeX/resumen_global1/imagenes/1.jpg El primer meme del .tex
│       │   ├── manejo-de-datos-20211/LaTeX/resumen_global1/imagenes/2.jpg El segundo meme del .tex
│       │   └── manejo-de-datos-20211/LaTeX/resumen_global1/imagenes/3.jpg El tercer meme del .tex
│       ├── manejo-de-datos-20211/LaTeX/resumen_global1/resumen_global.pdf El pdf de la segunda tarea
│       └── manejo-de-datos-20211/LaTeX/resumen_global1/resumen_global.texLa segunda tarea opcional dejada por sandra, fue un resumen con palabras propias de lo que habiamos visto y despues unos wenos mimos de programación
├── manejo-de-datos-20211/modulos
│   └── manejo-de-datos-20211/modulos/readme.txt Archivo donde contiene la descripción del directorio
├── manejo-de-datos-20211/README.md Archivo donde contiene la descripción del directorio
├── manejo-de-datos-20211/scripts
│   ├── manejo-de-datos-20211/scripts/AplicacionPOO.py Es la aplicación de la clase o el problema que elegimos, en mi caso fue el método de rechazo, o sea montecarlo para integrales y con el método aleatorio para elegir ua función
│   ├── manejo-de-datos-20211/scripts/Crear_y_poblarBaseDeDatos.py Con este archivo se usó para crear y poblar la base de datos(halm) con las preguntas y respuestas y las palabras que se habían pedido antes
│   ├── manejo-de-datos-20211/scripts/GráficaArañaOk.py En este programa el profe implementó gráficas de araña para distintas funciones
│   ├── manejo-de-datos-20211/scripts/main.py programa en el que se importaron elementos de las clases "racionales" y "complejos" para trabajarlos en el mismo archivo.
│   ├── manejo-de-datos-20211/scripts/MCD.py Programa donde se implementaron el algoritmo de euclides y el ciclo for para obtener MCD
│   ├── manejo-de-datos-20211/scripts/OrdenarListasdeListas.py En este programa se ordenaron listas de listas, se usó la función sorted, keys, funciones anonimas y se usó el depurador
│   ├── manejo-de-datos-20211/scripts/Ordenar.py  En este programa se trabajó con algoritmos de ordenamiento como el de la burbuja y la burbuja bidireccional
│   ├── manejo-de-datos-20211/scripts/ProblemaDelTransporteOK.py   Script que junto con la clase ProblemaDelTransporte.py se usó para resolver el problema de transporte, aquí se crearon los objetos de las clases usadas y se obtuvó la solución con cada uno de los métodos definidos en las clase "problema_transporte", ciudades USA.
│   ├── manejo-de-datos-20211/scripts/readme.txt Archivo donde contiene la descripción del directorio
│   ├── manejo-de-datos-20211/scripts/Recursividad.py En este programa se trabajaron(repasaron) la recursividad con los ejemplos de Fibonacci, factorial y las sumas
│   ├── manejo-de-datos-20211/scripts/Sesion25Septiembre.py En este programa se incluyó la clase "racionales" y la función "mcd"
│   ├── manejo-de-datos-20211/scripts/SQL01.py 1er archivo de MySQL que nos envió el profe
│   ├── manejo-de-datos-20211/scripts/SQL02.py 2do archivo de MySQL que nos envió el profe
│   ├── manejo-de-datos-20211/scripts/SQL03.py 3er archivo de MySQL que nos envió el profe
│   ├── manejo-de-datos-20211/scripts/SQL04.py 4to archivo de MySQL que nos envió el profe
│   ├── manejo-de-datos-20211/scripts/SQL05.py 5to archivo de MySQL que nos envió el profe
│   └── manejo-de-datos-20211/scripts/ValoresAleatorios.py  Script con el que se generaron valores aleatorios con random, seed etc.
├── manejo-de-datos-20211/Sesion25Sep.py
├── manejo-de-datos-20211/sql
│   ├── manejo-de-datos-20211/sql/Crear_y_poblarBaseDeDatos.py
│   ├── manejo-de-datos-20211/sql/readme.txt Archivo donde contiene la descripción del directorio
│   ├── manejo-de-datos-20211/sql/SQL01.py 1er archivo de MySQL que nos envió el profe
│   ├── manejo-de-datos-20211/sql/SQL02.py 2do archivo de MySQL que nos envió el profe
│   ├── manejo-de-datos-20211/sql/SQL03.py 3er archivo de MySQL que nos envió el profe
│   ├── manejo-de-datos-20211/sql/SQL04.py 4to archivo de MySQL que nos envió el profe
│   └── manejo-de-datos-20211/sql/SQL05.py 5to archivo de MySQL que nos envió el profe
└── manejo-de-datos-20211/tareas
    ├── manejo-de-datos-20211/tareas/Complejos En este archivo de texto donde se plantearon características de los números complejos para posibles métodos y atributos que se pueden implementar en la clase de complejos
    ├── manejo-de-datos-20211/tareas/ExamenFinal.py Es el archivo que contiene las preguntas con las respuestas hechas con material del curso
    ├── manejo-de-datos-20211/tareas/PalabrasManejoDeDatos.jpg foto de las palabras antes de pedir que se hiciera en otor formato
    ├── manejo-de-datos-20211/tareas/PalabrasManejoDeDatos.py Son las palabras por semanas obtenidas de los resumenes semanales
    ├── manejo-de-datos-20211/tareas/PreguntasManejoDeDatos1.jpg foto de las preguntasa antes de que se cambiara el formato
    ├── manejo-de-datos-20211/tareas/PreguntasManejoDeDatos2.jpg foto de las preguntasa antes de que se cambiara el formato
    ├── manejo-de-datos-20211/tareas/readme.txt Archivo donde contiene la descripción del directorio
    └── manejo-de-datos-20211/tareas/AplicaciónPOO
        ├── manejo-de-datos-20211/tareas/AplicaciónPOO/POO1.jpg Imagen con la aplicación de la clase creada para la parte de POO
	├── manejo-de-datos-20211/tareas/AplicaciónPOO/POO2.jpg Imagen con la aplicación de la clase creada para la parte de POO
	├── manejo-de-datos-20211/tareas/AplicaciónPOO/POO3.jpg Imagen con la aplicación de la clase creada para la parte de POO
	├── manejo-de-datos-20211/tareas/AplicaciónPOO/POO4.jpg Imagen con la aplicación de la clase creada para la parte de POO
	├── manejo-de-datos-20211/tareas/AplicaciónPOO/POO5.jpg Imagen con la aplicación de la clase creada para la parte de POO
	└── manejo-de-datos-20211/tareas/AplicaciónPOO/POO6.jpg Imagen con la aplicación de la clase creada para la parte de POO
15 directories, 65 files *el repositorio se edito posteriormente y se agregaron nuevos archivos
